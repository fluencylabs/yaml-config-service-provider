<?php
/**
 * PHP version >=5.5
 *
 * @category Component
 * @package  Fluency\Silex\Component\Yaml
 * @author   Rafael Ernesto Espinosa Santiesteban <rernesto.espinosa@gmail.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://fluency.inc.com
 */

namespace Fluency\Silex\Component\Yaml;


use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

/**
 * Class ConfigurationParser
 *
 * @category Component
 * @package  Fluency\Silex\Provider
 * @author   Rafael Ernesto Espinosa Santiesteban <rernesto.espinosa@gmail.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://fluency.inc.com
 */
class ConfigurationParser
{

    protected $_configSettings = array();
    protected $_vars;


    /**
     * Merge configurations arrays recursively. Overwrite values on $config1
     * with $config2 if already exists.
     *
     * @param array $config1 Original configuration
     * @param array $config2 Configuration to be merged
     *
     * @return array
     */
    protected function mergeConfigurations(array $config1, array $config2)
    {
        $merged = $config1;
        foreach ($config2 as $key => $value) {
            if (is_array($value)
                && isset ($merged [$key]) && is_array($merged [$key])
            ) {
                $merged [$key] = $this->mergeConfigurations($merged [$key], $value);
            } else {
                $merged [$key] = $value;
            }
        }
        return $merged;
    }

    /**
     * Parses imports statement on YML file.
     *
     * @param array  $imports    Imported YML files to be parsed
     * @param string $configPath Path to master YML file
     *
     * @return void
     */
    protected function parseImports(array $imports, $configPath)
    {
        foreach ($imports as $import) {
            $config = $this->parse(dirname($configPath) . '/' .$import['resource']);
            if ($config !== null) {
                $this->_configSettings = $this->mergeConfigurations(
                    $this->_configSettings, $config
                );
            }
        }
    }
    /**
     * Parses YAML into a PHP array.
     *
     * @param string $input                  Path to file or string containing YAML
     * @param bool   $exceptionOnInvalidType True if an exception must be thrown on
     *                                       invalid types false otherwise
     * @param bool   $objectSupport          True if object support is enabled,
     *                                       false otherwise
     *
     * @return array The YAML converted to a PHP array
     *
     * @throws ParseException If the YAML is not valid
     */
    protected function parse($input, $exceptionOnInvalidType = false,
                             $objectSupport = false
    ) {
        // if input is a file, process it
        $file = '';
        if (strpos($input, "\n") === false && is_file($input)) {
            if (false === is_readable($input)) {
                throw new ParseException(
                    sprintf(
                        'Unable to parse "%s" as the file is not readable.', $input
                    )
                );
            }
            $file = $input;
            $input = file_get_contents($file);
        }
        $input = str_replace(
            array_keys($this->_vars), array_values($this->_vars), $input
        );
        $yaml = new Parser();
        try {
            return $yaml->parse($input, $exceptionOnInvalidType, $objectSupport);
        } catch (ParseException $e) {
            if ($file) {
                $e->setParsedFile($file);
            }
            throw $e;
        }
    }
    /**
     * Sets YML parameters
     *
     * @return void
     */
    protected function setYamlPatameters()
    {
        foreach ($this->_configSettings['parameters'] as $key => $value) {
            $this->_vars['%'.$key.'%'] = $value;
        }
    }
}