<?php
/**
 * PHP version >=5.5
 *
 * @category ServiceProvider
 * @package  Fluency\Silex\Provider
 * @author   Rafael Ernesto Espinosa Santiesteban <rernesto.espinosa@gmail.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://fluency.inc.com
 */

namespace Fluency\Silex\Provider;

use Fluency\Silex\Component\Yaml\ConfigurationParser;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

/**
 * Class YamlConfigServiceProvider
 *
 * @category ServiceProvider
 * @package  Fluency\Silex\Provider
 * @author   Rafael Ernesto Espinosa Santiesteban <rernesto.espinosa@gmail.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://fluency.inc.com
 */
class YamlConfigServiceProvider extends ConfigurationParser
    implements ServiceProviderInterface, BootableProviderInterface
{

    /**
     * Class constructor. Set config paths and binding variables.
     *
     * @param array $vars Configuration variables
     */
    public function __construct(array $vars = array())
    {
        $this->_vars = $vars;
    }

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $app A container instance
     */
    public function register(Container $app)
    {
        $app['config'] =
            function () use ($app) {
                if (!isset($app['config.parameters'])) {
                    $app['config.parameters'] = 'parameters.yml';
                }
                $this->_configSettings = $this->parse(
                    $app['config.dir'] . '/' .
                    $app['config.parameters']
                );
                $this->setYamlPatameters();
                $this->_configSettings['base_path'] = $this->_vars['%base_path%'];
                $this->_configSettings['log_path'] = $this->_vars['%log_path%'];
                $this->_configSettings['cache_path'] = $this->_vars['%cache_path%'];
                foreach ($app['config.files'] as $fileName) {
                    $configPath = $app['config.dir'] . '/' . $fileName;
                    $config = $this->parse($configPath);
                    if (isset($config['imports']) && is_array($config['imports'])) {
                        $this->parseImports($config['imports'], $configPath);
                    }
                    if ($config !== null) {
                        $this->_configSettings = $this->mergeConfigurations(
                            $this->_configSettings, $config
                        );
                    }
                }
                return $this->_configSettings;
            };
    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     *
     * @param Application $app
     */
    public function boot(Application $app)
    {
        // TODO: Implement boot() method.
    }
}